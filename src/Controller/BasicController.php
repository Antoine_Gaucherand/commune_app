<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class BasicController extends AbstractController
{
    public function home()
    {
        return $this->render('home/home.html.twig');
    }

    public function upload()
    {
        return $this->render('upload/upload.html.twig');
    }

    public function status()
    {
        return $this->render('status/status.html.twig');
    }

    public function commwebserviceunes()
    {
        return $this->render('webservice/webservice.html.twig');
    }

    public function search()
    {
        return $this->render('search/search.html.twig');
    }
}


