<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use App\Document\User;
use Doctrine\ODM\MongoDB\DocumentManager as DocumentManager;
use MongoDB\Driver;
use MongoDB;
use MongoClient;
use Nelmio\ApiDocBundle\Annotation as Doc;

class MongoController
{

    public function mongoCreation()
    {
        ini_set('memory_limit', '2048M');
/*
        $ndjsongz=file_get_contents('https://adresse.data.gouv.fr/data/ban/adresses/latest/addok/adresses-addok-05.ndjson.gz');   //Récupère le fichier
        
        $ndjson=gzdecode($ndjsongz); //Décompresse le fichier

        //$bson=json_encode($ndjson);
        //$jsonarray2=json_decode($json, true);

        $bsonarray=explode("}",$ndjson, true);    //Scinde le fichier en tableau
        $json = MongoDB\BSON\toJSON(MongoDB\BSON\fromPHP($bsonarray));
        $jsonarray = json_decode($json);

        //var_dump($jsonarray);

        $connection = new MongoClient();    //Connection à Mongo
        $db = $connection->france;    //Création de la base MongoDB
        $collection = $db->adress05;    //Création de la collection dans la base et positionnement
        $collection->insert($jsonarray); //Ajout des données dans la collection
*/

        ///home/izork/Téléchargements/adresses-addok-05.ndjson
        $test=exec('mongoimport --db=test --collection=test_1 --file=/home/izork/Téléchargements/adresses-addok-france.ndjson');  //Mettre le bon chemin

        return new Response("Création base et ajout json OK");
    }

    /**
     * …
     *
     * @127.0.0.1/communes(
     *     resource=true,
     *     description="Get all DB"
     * )
     */

    public function mongoPrint()
    {
        $collection = (new MongoDB\Client)->test->test_1;

        $streetQuery = array('type' => 'street');
        $populationQuery = array('population'=> 107);

        //$testQuery = {_id: { $in: [ObjectId("5e96c5b11000ae32d0742d94") ] } } ;
        
        $cursor = $collection->find();
        
        foreach ($cursor as $doc) {
           var_dump($doc);
        };

        return new Response("blabla");
    }

    public function statusCheck()
    {

    $manager = new MongoDB\Driver\Manager('mongodb://localhost:27017');
    $command = new MongoDB\Driver\Command(['ping' => 1]);
    
    try {
        $cursor = $manager->executeCommand('test', $command);
        
        echo "Connection Success"."<br>";
    }
    catch(MongoDB\Driver\Exception $e) {
        echo $e->getMessage(), "\n";
        echo"Connection Failed";
        exit;
    }

    $response = $cursor->toArray()[0];
    
    var_dump($response);

    return new Response("");

    }
    
}